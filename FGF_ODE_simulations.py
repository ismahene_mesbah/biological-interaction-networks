
#parameter and initial condition definition; a dict is a must
import PyDSTool as pdt
params={'vs1': 0.2, 'kd1':0.2, 'kt1':0.5,'kd5':0.9, 'ks3': 0.1, 'km':5, 'kd4':0.6, 'kt2':0.1,'kr1':1, 'kr2':1, 'teta':0.1
   
       }
ini={'RA1':0.5,'F1':0, 'F2':1, 'RA2':1}

DSargs=pdt.args(name='SIRtest_multi',
                ics=ini,
                pars=params,
                tdata=[0,80],
                #the for-macro generates formulas ; 
                
                varspecs={'RA[o]':'for(o,1,2,vs1- kd1 *RA[o] -kd5*RA[o]- kt1*RA[o]+ teta*kr1*RA[o] )',
                          'F[l]':'for(l,1,2,ks3*(km/ (km+RA2)) - kd4*F[l] - kt2*F[l] + teta*F[l]*kr2)'})

#generator
DS = pdt.Generator.Vode_ODEsystem(DSargs)

#computation, a trajectory object is generated
trj=DS.compute('test')
#extraction of the points for plotting
pts=trj.sample()


#plotting; pylab is imported along with PyDSTool as plt
pdt.plt.plot(pts['t'],pts['RA1'],label='RA')
pdt.plt.plot(pts['t'],pts['F1'],label='F')

pdt.plt.legend()
pdt.plt.xlabel('t')
pdt.plt.title('Retinoic Acid and FGF protein evolution during time')
pdt.plt.show()
