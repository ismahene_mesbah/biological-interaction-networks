## Biological interactions networks

The goal of the project is to analyze and study FGF8 protein with 3 different approaches. All the approaches are detailed in the paper.

Three files are available in the current directory:

* '`import_network.R`' : this script was developed in R. It imports the static network of FGF8 interactions from PSICQUIC database.


* '`analyze network.R`' : this script was written in R language. It perfoms all the necessary analysis that help describing the static network of FGF8 protein already imported from PSICQUIC database. 

* '`FGF_ODE_simulations.py`' : this scipt was developed in python. It contains the differential equations that describe FGF8 evolution in time. Simulations can be perfomed and plots will be generated to figure out the steady states of the system.


